import React, { Component } from 'react';
import './App.css';
import Bars from 'react-bars';
import 'primereact/resources/primereact.min.css';
import 'primereact/resources/themes/omega/theme.css';
import {Slider} from 'primereact/components/slider/Slider';


class App extends Component {
  constructor() {
    super()
    this.state = {
      volume: [
        {id: '0', label: '50%',value:50, barColor:'green'}, 
        {id: '2', label: '50%', value:50, barColor:'green'}, 
        {id: '3', label: '50%', value:50, barColor:'green'}, 
        {id: '4', label: '50%', value:50, barColor:'green'}, 
        {id: '5', label: '50%', value:50, barColor:'green'}, 
        {id: '6', label: '50%',value:50, barColor:'green'},
        {id: '7', label: '50%', value:50, barColor:'green'}, 
        {id: '8', label: '50%', value:50, barColor:'green'}, 
        {id: '9', label: '50%', value:50, barColor:'green'}, 
        {id: '10', label: '50%', value:50, barColor:'green'}
      ]
    }
  }

  onChangeSlider(e, key) {

    let volumeArray = this.state.volume.slice();
    volumeArray[key].value = e.value;
    volumeArray[key].label = `${e.value}%`;
    this.setState({
      volume: volumeArray
    });
  }

  render() {
    return(
      <div>
      <Bars data={this.state.volume} makeUppercase={true}/> 
      {
        this.state.volume.map((elem, key)=>{
          return (
            <div key={key} className="sliderContainer">
              <h3>{elem.label}</h3>
              <Slider 
                key={elem.id} 
                value={elem.value} 
                orientation="vertical" 
                style={{ height: '200px' }} 
                onChange={(event)=>this.onChangeSlider(event,key)}
              />
            </div>
          )          
        })
      }
      </div>  
    )
      
  }
}

export default App;
